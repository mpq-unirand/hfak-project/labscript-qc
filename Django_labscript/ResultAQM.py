#!/usr/bin/env python
# coding: utf-8

import json
import os
import numpy as np
import time
import shutil
from lyse import *
import pandas as pd
import h5py

import analysislib.UnirandScripts.LabscriptScripts.AbsImaging as AbsImaging

def get_spin_up_down_atoms(Atom_image,Ref_image,Dark_image):
    atoms_per_site = np.arange(1e4,4e4,1e4)
    return atoms_per_site, 0.1*atoms_per_site

def store_result(shot_path):
    with h5py.File(shot_path, 'r+') as f:
        abs_image = AbsImaging.AbsImage()
        abs_image.__int__()

        atom_image = np.array(f['images']['Alvium1']['ABS']['atoms'])
        probe_image = np.array(f['images']['Alvium1']['ABS']['probe'])
        dark_image = np.array(f['images']['Alvium1']['ABS']['background'])
        image_data = np.nan_to_num(np.log((probe_image - dark_image) / (atom_image - dark_image)), neginf=0.0, nan=0.0)
        image_data[image_data >= 1] = 0
        abs_image.data = image_data
        abs_image.size = np.shape(image_data)
        abs_image.region_of_interest(x1=620,x2=1400,y1=420,y2=800)
        try:
            abs_image.guess_initial_values()
            abs_image.fit()
            n_atoms = 4 * np.pi * abs_image.fit_result['amplitude'] * np.abs(abs_image.fit_result['sigma_x']) * np.abs(
                abs_image.fit_result['sigma_y']) * (14.4e-6) ** 2 / (0.671e-6) ** 2
        except:
            n_atoms = 'Failed calculation'

        try:
            analyis_results_group = f.create_group('results/AtomNumber')
        except:
            analyis_results_group = f['results/AtomNumber']
        analyis_results_group.attrs.create('AtomNumber', data=n_atoms)
        # for i in range(Spin_up.size):
        #     site=str(i)
        #     atom_num_arr = np.array((Spin_up[i],Spin_down[i]))
        #     analyis_results_group.attrs.create('Wire_'+site, data=atom_num_arr, shape=np.shape(atom_num_arr))

def move_to_sds(shot_path,running_file_path,finished_file_path):
    dst_path='/home/superuser/Documents/Software/HFAK/Job_management/dist_loc/'+shot_path[57:]
    dst_dir=dst_path.rpartition('/')[0]
    os.makedirs(dst_dir, exist_ok=True)
    shutil.move(shot_path, dst_path)
    shutil.move(running_file_path,finished_file_path)
    return dst_path

def check_job_done(selected_job_id,job_folder_for_csv):
    original_job_folder='/'+job_folder_for_csv[1:]
    last_sub_folder=sorted(os.listdir(original_job_folder))[-1]#next(os.walk(job_folder_for_csv))[1][-1]
    last_sub_folder_path=os.path.join(original_job_folder,last_sub_folder)
    num_remaining_shots=len(os.listdir(last_sub_folder_path))#len(next(os.walk(last_sub_folder_path))[2])
    print(last_sub_folder_path)
    print(num_remaining_shots)
    job_done=True
    if not num_remaining_shots:
        job_done=True
    return job_done

def gen_multishot_CSVs(job_folder_for_csv):
    subfolder_list=next(os.walk(job_folder_for_csv))[1]
    shot_subfolder_paths=[os.path.join(job_folder_for_csv,fn) for fn in subfolder_list]
    for i in range(len(shot_subfolder_paths)):
        file_list=list(fn for fn in next(os.walk(shot_subfolder_paths[i]))[2])
        shot_paths = [os.path.join(shot_subfolder_paths[i],fn) for fn in file_list]
        df=dataframe_utilities.get_dataframe_from_shots(shot_paths)
        df.columns = ['_'.join(tup).rstrip('_') for tup in df.columns.values]
        path_csv= os.path.join(job_folder_for_csv,'df_'+subfolder_list[i]+'.csv')
        df.to_csv(path_csv, index=False)

def create_memory_data(df,name):
    exp_sub_dict = {
            'header': {'name': 'experiment_0', 'extra metadata': 'text'},
            'shots': 3,
            'success': True,
            'data': {      # slot 1 (Na)      # slot 2 (Li)
                'memory': None
            }
        }
    exp_sub_dict['header']['name']=name
    exp_sub_dict['shots']=df.shape[0]
    memory_df=df.loc[:,df.columns.str.startswith("AtomNumber")]
    l=list(memory_df.astype(str).values.sum(axis=1))
    memory_list=[w.replace('][', ' ; ').replace(',', '').replace('[', '').replace(']', '') for w in l]
    exp_sub_dict['data']['memory'] = memory_list
    return exp_sub_dict

def create_json_result(selected_job_id,job_folder_for_csv):
    result_dict = {
    'backend_name': 'SoPa_atomic_mixtures',
    'backend_version': '0.0.1',
    'job_id': 'None',
    'qobj_id': None,
    'success': True,
    'status': 'finished',
    'header': {},
    'results': []}
    result_dict['job_id']=selected_job_id
    exp_list = next(os.walk(job_folder_for_csv))[1]
    for i in range(len(exp_list)):
        path_csv= os.path.join(job_folder_for_csv,'df_'+exp_list[i]+'.csv')
        df = pd.read_csv(path_csv)
        result_dict['results'].append(create_memory_data(df,exp_list[i]))
    return result_dict

def change_json_status_to_done(json_status_folder,selected_job_id, status_folder):
    id = selected_job_id.split("_")[1]
    for dirpath, dirnames, filenames in os.walk(status_folder):
        for filename in [f for f in filenames if id in f]:
            status_file_name = filename
            status_user_folder = dirpath
    if not status_file_name:
        raise Exception('Status json file not found.')
    # status_file_name = 'status-'+selected_job_id+'.json'
    status_file_path = os.path.join(status_user_folder, status_file_name)
    status_msg_dict = {'job_id': 'None','status': 'None','detail': 'None'}
    with open(status_file_path) as status_file:
        status_msg_dict = json.load(status_file)
    status_msg_dict['detail'] += '; BLACS and analysis done. Results are available!'
    status_msg_dict['status'] = 'DONE'
    with open(status_file_path, 'w') as status_file:
        json.dump(status_msg_dict, status_file)
        #status_file.flush()
        #os.fsync(status_file.fileno())

def do_analysis():
    job_dict_file=R'/home/superuser/Documents/Software/HFAK/Job_management/Running/multi_analysis_dict.json'
    running_folder=R'/home/superuser/Documents/Software/HFAK/Job_management/Running'
    finished_folder=R'/home/superuser/Documents/Software/HFAK/Job_management/Finished'
    result_json_folder=R'/home/superuser/Documents/Software/HFAK/Dropbox/Apps/qlue/Backend_files/Result'
    json_status_folder = R'/home/superuser/Documents/Software/HFAK/Dropbox/Apps/qlue/Backend_files/Status'
    keep_analysing_file=R'/home/superuser/Documents/Software/HFAK/Job_management/keep_analysing.txt'
    status_folder=R'/home/superuser/Documents/Software/HFAK/Dropbox/Apps/qlue/Backend_files/Status/mot'
    while True:
        time.sleep(2)
        if not os.path.isfile(keep_analysing_file):
            print('You must create a keep_analysing file')
            break

        with open(job_dict_file, 'r') as f:
            job_dict = json.load(f)
        job_id_list=list(job_dict.keys())
        file_name=list(fn for fn in next(os.walk(running_folder))[2] if fn!='multi_analysis_dict.json')#sort this list
        if file_name:
            file_name=sorted(file_name)[0]
            running_file_path = os.path.join(running_folder,file_name)
            finished_file_path = os.path.join(finished_folder,file_name)
            with open(running_file_path, "r") as text_file:
                shot_path=text_file.read()

            store_result(shot_path)
            shot_path=move_to_sds(shot_path,running_file_path,finished_file_path)

            current_job_id=file_name.split('-')[0]
            current_job_id_folder=os.path.join(shot_path.split(current_job_id)[0],current_job_id)

            # if not current_job_id in job_id_list:
            job_dict[current_job_id]=shot_path[:111] #current_job_id_folder

        job_id_list=list(job_dict.keys())
        job_done=False
        if job_id_list:
            selected_job_id=job_id_list[0]
            job_folder_for_csv = job_dict[selected_job_id]
            job_done=check_job_done(selected_job_id,job_folder_for_csv)
        if job_done:
            gen_multishot_CSVs(job_folder_for_csv)

            id = selected_job_id.split("_")[1]
            for dirpath, dirnames, filenames in os.walk(status_folder):
                for filename in [f for f in filenames if id in f]:
                    results_id = filename[7:-5]
                    results_user = filename[27:-11]

            result_dict=create_json_result(selected_job_id,job_folder_for_csv)
            isExist = os.path.exists(f'{result_json_folder}/mot/{results_user}')
            if not isExist:
                os.makedirs(f'{result_json_folder}/mot/{results_user}')
            result_json_path=os.path.join(f'{result_json_folder}/mot/{results_user}','result-'+results_id+'.json')
            with open(result_json_path, 'w') as f:
                json.dump(result_dict, f)
            change_json_status_to_done(json_status_folder,selected_job_id, status_folder)
            del job_dict[selected_job_id]

        with open(job_dict_file, 'w') as f:#turn off write buffer here
            json.dump(job_dict, f)
            #f.flush()
            #os.fsync(f.fileno())

do_analysis()
